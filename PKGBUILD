# Maintainer: Morten Linderud <foxboron@archlinux.org>
# Maintainer: George Rawlinson <grawlinson@archlinux.org>
# Contributor: Maikel Wever <maikelwever@gmail.com>
# Contributor: Asterios Dimitriou <asterios@pci.gr>
# Contributor: Benjamin Asbach <archlinux-aur.lxd@impl.it>
# Contributer: nightuser <nightuser.android at gmail.com>

pkgname=incus
pkgver=0.4
pkgrel=1
pkgdesc="Powerful system container and virtual machine manager"
arch=('x86_64')
url="https://linuxcontainers.org/incus/"
license=('Apache-2.0')
depends=('lxc' 'lxcfs' 'squashfs-tools' 'dnsmasq' 'cowsql' 'libuv' 'ebtables' 'raft' 'sqlite' 'libcap' 'acl')
makedepends=('go' 'git' 'tcl' 'apparmor' 'libseccomp' 'systemd')
optdepends=(
    'lvm2: lvm2 support'
    'thin-provisioning-tools: thin provisioning support'
    'btrfs-progs: btrfs storage driver support'
    'minio: storage buckets support'
    'cdrtools: VM support'
    'qemu: VM support'
    'edk2-ovmf: VM support'
    'systemd-libs: unix device hotplug support'
    'apparmor: apparmor support'
)
source=("https://github.com/lxc/incus/releases/download/v${pkgver}.0/${pkgname}-${pkgver}.tar.xz"{,.asc}
        "incus.socket"
        "incus.service"
        "incus-user.socket"
        "incus-user.service"
        "incus.sysusers")
validpgpkeys=('602F567663E593BCBD14F338C638974D64792D67'  # Stéphane Graber <stgraber@stgraber.org>
             )
sha256sums=('bee802ade967402b14401e1b5ecbd1336b007a4d816bd86644abbb43b9fc1845'
            'SKIP'
            '602a8035cc51b8e0c4ca265e31ebf96c7977db4239d384d8f60fed0d2d0317c4'
            '09936fdbafe493687727e3b2f1e9509258295471f922784968b9a650fc48b0ea'
            '50288528ea910d186157a2515c5200b3b073f645922fb581d1913f236f14667d'
            '1c934cac1aa61be2ede5830ea76dbb9ba199498f6eb920ce1016bbb54dd088bf'
            '90beead5dbde947936a58df8773519c49d92a2028881ae0ec81ee5402510e39a')

prepare() {
  cd "$pkgname-$pkgver"

  mkdir bin
  go mod verify
}

build() {
  cd "$pkgname-$pkgver"

  export GOFLAGS="-buildmode=pie -modcacherw"
  export CGO_LDFLAGS_ALLOW="-Wl,-z,now"
  GO_LDFLAGS="-compressdwarf=false -linkmode external"

  CGO_LDFLAGS="-static" go build -v -tags "agent" -o bin/ ./cmd/incus-agent/...

  go build -v -ldflags "${GO_LDFLAGS}" -tags "netgo" -o bin/ ./cmd/incus-migrate/...
  for tool in fuidshift incus lxc-to-incus incusd incus-benchmark incus-user; do
    go build -v -ldflags "${GO_LDFLAGS}" -tags "libsqlite3" -o bin/ ./cmd/$tool
  done

  # Generate manpages - not worth it
  # mkdir manpages
  # ./bin/incus manpage --all ./manpages
  # ./bin/incusd manpage --all ./manpages

  # Build lxd-to-incus as separate binary
  cd cmd/lxd-to-incus
  go build -v -ldflags "${GO_LDFLAGS}" -tags "libsqlite3" -o ../../bin/
}

package() {
  cd "$pkgname-$pkgver"

  for tool in fuidshift lxd-to-incus; do
    install -v -p -Dm755 "bin/$tool" "${pkgdir}/usr/sbin/$tool"
  done
  for tool in incus lxc-to-incus incusd incus-agent incus-benchmark incus-user; do
    install -v -p -Dm755 "bin/$tool" "${pkgdir}/usr/bin/$tool"
  done

  # Package license
  install -v -Dm644 "COPYING"  "${pkgdir}/usr/share/licenses/${pkgname}/LICENCE"

  # systemd files
  install -v -Dm644 "${srcdir}/"incus.{service,socket} -t "${pkgdir}/usr/lib/systemd/system"
  install -v -Dm644 "${srcdir}/"incus-user.{service,socket} -t "${pkgdir}/usr/lib/systemd/system"
  install -v -Dm644 "${srcdir}/$pkgname.sysusers" "${pkgdir}/usr/lib/sysusers.d/$pkgname.conf"

  # logs
  install -v -dm700 "${pkgdir}/var/log/incus"

  # documentation
  install -d "${pkgdir}/usr/share/doc/incus/"
  rm -rf doc/html
  cp -vr doc/* "${pkgdir}/usr/share/doc/incus/"

  # Bash completions
  install -v -p -Dm644 "scripts/bash/incus" "${pkgdir}/usr/share/bash-completion/completions/incus"
}

# vim:set ts=2 sw=2 et:
